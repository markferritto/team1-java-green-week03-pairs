package com.techelevator.TollCalculator;

public class Car implements Vehicle {

    private boolean hasTrailer;

    public Car () {

    }

    public Car(boolean hasTrailer) {
        this.hasTrailer = false;
    }

    public boolean getHasTrailer() {
        return hasTrailer;
    }

    @Override
    public double calculateToll(int distance) {
        if(hasTrailer) {
            return distance * 0.020;
        }else{
            return distance += 1;
        }
    }
}
