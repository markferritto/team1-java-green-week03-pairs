package com.techelevator.TollCalculator;

public interface Vehicle {

    double calculateToll(int distance);


}
