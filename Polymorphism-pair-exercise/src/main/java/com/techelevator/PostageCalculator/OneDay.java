package com.techelevator.PostageCalculator;

public class OneDay implements DeliveryDriver{

    @Override
    public double calculateRate(int distance, double weight) {
        return (weight * 0.075);
    }
}
