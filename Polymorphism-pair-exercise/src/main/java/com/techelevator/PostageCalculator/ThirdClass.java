package com.techelevator.PostageCalculator;

public class ThirdClass extends PostalService{

    @Override
    public double calculateRate(int distance, double weight) {

        if(weight <= 2) {
            return distance * 0.0020;
        } else if (weight > 2 && weight <= 8) {
            return distance * 0.0022;
        } else if (weight >= 9 && weight <= 15) {
            return distance * 0.0024;
        } else if (weight >= 16 && weight <= 48) {
            return distance * 0.0150;
        } else if (weight >= 64 && weight <= 128) {
            return distance * 0.0160;
        }else {
            return distance * 0.0170;
        }

    }



}
