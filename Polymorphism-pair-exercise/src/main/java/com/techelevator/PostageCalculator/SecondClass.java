package com.techelevator.PostageCalculator;

public class SecondClass extends PostalService {

    @Override
    public double calculateRate(int distance, double weight) {

        if(weight <= 2) {
            return distance * 0.0035;
        } else if (weight > 2 && weight <= 8) {
            return distance * 0.0040;
        } else if (weight >= 9 && weight <= 15) {
            return distance * 0.0047;
        } else if (weight >= 16 && weight <= 48) {
            return distance * 0.0195;
        } else if (weight >= 64 && weight <= 128) {
            return distance * 0.0450;
        }else {
            return distance * 0.0500;
        }

    }


}
