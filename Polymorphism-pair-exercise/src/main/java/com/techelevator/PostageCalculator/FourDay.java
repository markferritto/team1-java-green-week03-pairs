package com.techelevator.PostageCalculator;

public class FourDay implements DeliveryDriver {

    @Override
    public double calculateRate(int distance, double weight) {
        return (weight * 0.0050) * distance;
    }


}
