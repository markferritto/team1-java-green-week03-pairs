package com.techelevator.PostageCalculator;

public class FirstClass extends PostalService{

    @Override
    public double calculateRate(int distance, double weight) {

        if(weight <= 2) {
            return distance * 0.035;
        } else if (weight > 2 && weight <= 8) {
            return distance * 0.040;
        } else if (weight >= 9 && weight <= 15) {
            return distance * 0.047;
        } else if (weight >= 16 && weight <= 48) {
            return distance * 0.195;
        } else if (weight >= 64 && weight <= 128) {
            return distance * 0.450;
        }else {
            return distance * 0.500;
        }

    }
}
